import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import swal from 'sweetalert';
import { DataService } from "../data.service";
 
@Component({
  selector: 'app-sudoku-board',
  templateUrl: './sudoku-board.component.html',
  styleUrls: ['./sudoku-board.component.scss']
})
export class SudokuBoardComponent implements OnInit {
  public board_cells: number[][];
  private progress_board: number[][];
  private solved_board: number[][];
  public board_size: number = 9;

  constructor(private http_client: HttpClient, private data: DataService) {
    this.getBoard();
  }

  onInput({cell_id, input_num}) {
    this.progress_board[Math.floor(cell_id / this.board_size), cell_id % this.board_size] = input_num;
    let element = (<HTMLInputElement>document.getElementById(cell_id));
    element.value = input_num;
    this.updateProgress(element, Number(input_num));
  }

  onInputChange(event) {
    let input_num = Number(event.key);
    if (event.srcElement == "") {
      this.updateProgress(event, 0);
    }
    else if (isNaN(input_num)) {
        event.srcElement.value = "";
        event.srcElement.style.color = "black";
    }
    else {
      this.updateProgress(event.srcElement, input_num);
    }
  }

  updateProgress(element, input_num) {
    let id = element.id;
    let row = Math.floor(id / this.board_size);
    let col = id % this.board_size;
    if (!this.checkIfSafe(row, col, input_num )) {
      element.style.color = "red";
    } else {
      element.style.color = "black";
      this.progress_board[row][col] = input_num;
      if (this.checkSolution()) {
        swal("Good job!", "You've solved the board!", "success");
        this.getBoard();
      }
    }
  }

  checkSolution(): boolean {
    for (let row = 0; row < this.board_size; row++) {
      for (let col = 0; col < this.board_size; col++) {
        if (!(this.progress_board[row][col] == this.solved_board[row][col])) {
          return false;
        }
      }
    }
    return true;
  }

  fillBoard(raw_board: string): number[][] {
    let array: number[][];
    array = [];

    for (let row = 0; row < this.board_size; row++) {
      array[row] = [];
      for (let col = 0; col < this.board_size; col++) {
        array[row][col] = parseInt(raw_board[row * this.board_size + col]);
      }
    }
    return array;
  }

  getBoard(): void {
    this.http_client.get('assets/sudoku.csv', { responseType: 'text' })
    .subscribe(data => {
      let boards_array = data.split('\n');
      let board_idx = this.getRandomInt(boards_array.length);

      this.board_cells = this.fillBoard(boards_array[board_idx].split(',')[0]);
      this.progress_board = this.fillBoard(boards_array[board_idx].split(',')[0]);
      this.solved_board = this.fillBoard(boards_array[board_idx].split(',')[1]);
    });
  }

  checkIfSafe(row: number, col: number, num: number): boolean { 
    return (this.unUsedInRow(row, num) && 
            this.unUsedInCol(col, num) && 
            this.unUsedInBox(row - row % 3, col - col % 3, num)); 
  }
  
  unUsedInBox(rowStart: number, colStart: number, num: number): boolean { 
    for (let row = 0; row < 3; row++) { 
      for (let col = 0; col < 3; col++) { 
        if ((this.board_cells[rowStart + row][colStart + col] == num) || (this.progress_board[rowStart + row][colStart + col]) == num)
          return false; 
      }
    }
    return true; 
  } 
  
  unUsedInRow(row: number, num: number): boolean { 
    for (let col = 0; col < this.board_size; col++) 
      if ((this.board_cells[row][col] == num) || (this.progress_board[row][col]) == num)
        return false; 
    return true; 
  } 
   
  unUsedInCol(col: number, num: number): boolean { 
    for (let row = 0; row < this.board_size; row++) 
      if ((this.board_cells[row][col] == num) || (this.progress_board[row][col]) == num)
        return false; 
    return true; 
  } 

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  clearBoard() {
    for (let row = 0; row < this.board_size; row++) {
      for (let col = 0; col < this.board_size; col++) {
        if (this.board_cells[row][col] == 0) {
          this.progress_board[row][col] = 0;
          (<HTMLInputElement>document.getElementById(String(row * this.board_size + col))).value = ""; 
        }
      }
    }
  }

  ngOnInit(): void {
    this.data.currentMessage.subscribe((message: string) => {
      if (message == "new") {
        this.getBoard();
      } else if (message == "clear") {
        this.clearBoard();
      }
    });
  }
}