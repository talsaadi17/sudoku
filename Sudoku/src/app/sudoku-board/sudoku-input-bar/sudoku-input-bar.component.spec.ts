import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SudokuInputBarComponent } from './sudoku-input-bar.component';

describe('SudokuInputBarComponent', () => {
  let component: SudokuInputBarComponent;
  let fixture: ComponentFixture<SudokuInputBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SudokuInputBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SudokuInputBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
