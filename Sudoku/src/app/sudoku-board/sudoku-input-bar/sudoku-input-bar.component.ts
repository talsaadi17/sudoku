import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sudoku-input-bar',
  templateUrl: './sudoku-input-bar.component.html',
  styleUrls: ['./sudoku-input-bar.component.scss']
})
export class SudokuInputBarComponent implements OnInit {
  public numbers: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  @Output() input = new EventEmitter<{cell_id: number, input_num: number}>();

  constructor() { }

  onClick(event) {
    if (document.activeElement.tagName == "INPUT") {
      let id = Number(document.activeElement.id);
      if (!isNaN(id)) {
        this.input.emit({cell_id: id, input_num: event.srcElement.id});
      }
    } 
  }

  ngOnInit(): void {
  }

}
