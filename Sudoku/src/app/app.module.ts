import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SudokuBoardComponent } from './sudoku-board/sudoku-board.component';
import { SudokuInputBarComponent } from './sudoku-board/sudoku-input-bar/sudoku-input-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataService } from "./data.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SudokuBoardComponent,
    SudokuInputBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
