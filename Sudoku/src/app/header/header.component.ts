import { Component, OnInit, Input } from '@angular/core';
import swal from 'sweetalert';
import { DataService } from "../data.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title: string;

  constructor(private data: DataService) { }

  onClearBoard() {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this board!",
      icon: "warning",
      dangerMode: true,
      buttons: ["Cancel", "Yes"],
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Poof! Your board has been deleted!", {
          icon: "success",
        });
        this.data.changeMessage("clear");
      } else {
        swal("Your board is safe!");
      }
    });
  }

  onNewGame() {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this board!",
      icon: "warning",
      dangerMode: true,
      buttons: ["Cancel", "Yes"],
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Poof! New game is ready!", {
          icon: "success",
        });
        this.data.changeMessage("new");     
      } else {
        swal("Your board is safe!");
      }
    });
  }

  ngOnInit(): void {
    this.data.currentMessage.subscribe(message => message);
  }
}
